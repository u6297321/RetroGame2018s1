package nameless.jumper;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by zhang on 2018/5/15.
 *
 * This class represents un-operable objects for players.
 * e.g. floor, wall or any other obstructions.
 */

public class Block extends MovableObject implements Obstruction{
    private Bitmap bitmap;

    Block(Bitmap bitmap, float startX, float startY){
        super(startX, startY);
        this.bitmap = bitmap;
    }

    @Override
    public boolean collisionDetect(MovableObject object) {
        float xR = object.getCenterX();
        float yR = object.getCenterY();

        float rightEdge = currentX + bitmap.getWidth();
        float bottomEdge = currentY + bitmap.getHeight();

        if (xR > currentX && xR < rightEdge) {
            if (yR >= currentY) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void collisionHandle(MovableObject object, Context context) {
        if (collisionDetect(object)) {
            object.collisionReact(this);
        }
    }

    /**
     * Currently, blocks have nothing to do when collision detected.
     */
    @Override
    void collisionReact(MovableObject collisionWithObject) {
    }

    @Override
    public float getCenterX() {
        return currentX + bitmap.getWidth()/2;
    }

    @Override
    public float getCenterY() {
        return currentY + bitmap.getHeight()/2;
    }

    /**
     * GET method for bitmap
     */
    public Bitmap getBitmap() {
        return bitmap;
    }
}
