package nameless.jumper;

import android.graphics.Bitmap;

/**
 * Created by zhang on 2018/5/14.
 *
 * This class represents an operable object for players.
 * Basically in current version of this game, it looks like a "Rocket", so I call it "Rocket"
 */
public class Rocket extends MovableObject{
    // Bitmap
    private Bitmap bitmap;

    /**
     * Set a valid range for speeds.
     * This attribute should be seen as a kind of features of different movable objects.
     */
    private static final float[] vSpeedRange = {20.0f, 60.0f};
    private static final float[] hSpeedRange = {0.0f, 35.0f};

    Rocket(Bitmap bitmap, float startX, float startY) {
        super(startX, startY);

        this.bitmap = bitmap;
    }

    /**
     * Start motion and set initial speed.
     * @param factorX affects on initial vertical speed
     * @param factorY affects on initial horizontal speed
     */
    public void start(float factorX, float factorY) {
        if (!isMoving) {
            horizontalSpeed = hSpeedRange[0] + factorX*(hSpeedRange[1] - hSpeedRange[0]);
            verticalSpeed = -(vSpeedRange[0] + factorY*(vSpeedRange[1] - vSpeedRange[0]));
            System.out.println("HS:" + horizontalSpeed);
            System.out.println("VS:" + verticalSpeed);

            isMoving = true;
        }
    }

    /**
     * Overload "start" method for default factorX = factorY = 0.0f
     */
    public void start() {
        start(0.0f, 0.0f);
    }

    /**
     * Stop motion
     */
    public void stop() {
        if (isMoving) {
            horizontalSpeed = 0.0f;
            verticalSpeed = 0.0f;
            isMoving = false;
        }
    }

    /**
     * Step: this method should be executed with View's refreshing.
     */
    public void step(float verticalAcceleration, float horizontalAcceleration) {
        if (isMoving) {
            currentX += (horizontalSpeed + horizontalAcceleration/2);
            horizontalSpeed += horizontalAcceleration;

            currentY += (verticalSpeed + verticalAcceleration/2);
            verticalSpeed += verticalAcceleration;
        }
    }

    /**
     * GET method for bitmap.
     */
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    void collisionReact(MovableObject collisionWithObject) {
        this.stop();
        currentY = collisionWithObject.getCurrentY() - bitmap.getHeight();
    }

    @Override
    public float getCenterX() {
        return currentX + bitmap.getWidth()/2;
    }

    @Override
    public float getCenterY() {
        return currentY + bitmap.getHeight();
    }
}
