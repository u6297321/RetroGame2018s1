package nameless.jumper;

/**
 * Created by zhang on 2018/5/15.
 *
 * This abstract class is the parent of all movable objects in this game (mainly it represents the Rocket).
 * Here are several definitions of fundamental motion variable and their GET methods and SET methods.
 */

public abstract class MovableObject {
    // Motion parameters
    float currentX;
    float currentY;

    float verticalSpeed;
    float horizontalSpeed;

    boolean isMoving;

    MovableObject(float startX, float startY) {
        this.verticalSpeed = 0.0f;
        this.horizontalSpeed = 0.0f;

        this.currentX = startX;
        this.currentY = startY;

        this.isMoving = false;
    }

    MovableObject() {
        this.verticalSpeed = 0.0f;
        this.horizontalSpeed = 0.0f;

        this.currentX = 0.0f;
        this.currentY = 0.0f;

        this.isMoving = false;
    }

    /**
     * GET methods
     */

    public boolean isMoving() {
        return isMoving;
    }

    public float getVerticalSpeed() {
        return verticalSpeed;
    }

    public float getHorizontalSpeed() {
        return horizontalSpeed;
    }


    public float getCurrentX() {
        return currentX;
    }

    public float getCurrentY() {
        return currentY;
    }

    /**
     * SET methods
     */

    public void setCurrentX(float targetX) {
        this.currentX = targetX;
    }

    public void setCurrentY(float targetY) {
        this.currentX = targetY;
    }

    /**
     * Center x should be a visual/reasonable position for the object.
     * Basically it could be the geometric center of the object.
     */
    abstract public float getCenterX();

    /**
     * Center y should be a visual/reasonable position for the object.
     * Basically it could be the geometric center of the object.
     */
    abstract public float getCenterY();

    /**
     * This method should by called by collisionHandle() method of Obstruction instances.
     * It is the action that movableObject should do when collision happened.
     *
     * @param collisionWithObject should be the Obstruction instance.
     */
    abstract void collisionReact(MovableObject collisionWithObject);
}
