package nameless.jumper;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;

/**
 * Created by zhang on 2018/5/18.
 *
 * This class represents a kind of Block that used for Fail judgement.
 * Basically it could be seen as "traps" or "forbidden areas" in this game.
 */

public class FailBlock extends Block {
    FailBlock(Bitmap bitmap, float startX, float startY) {
        super(bitmap, startX, startY);
    }

    /**
     * Rewrite collisionHandle() method to make a pop up tip for FAIL information.
     */
    @Override
    public void collisionHandle(MovableObject object, Context context) {
        if (collisionDetect(object)) {
            object.collisionReact(this);

            new  AlertDialog.Builder(context)
                    .setTitle("GAME OVER :(" )
                    .setPositiveButton("OK" ,  null )
                    .show();

            System.out.println("----- GAME OVER :( -----");
        }
    }
}
