package nameless.jumper;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;

/**
 * Created by zhang on 2018/5/18.
 *
 * This class represents a kind of Block that used for Win judgement.
 */

public class WinBlock extends Block {
    WinBlock(Bitmap bitmap, float startX, float startY) {
        super(bitmap, startX, startY);
    }

    /**
     * Rewrite collisionHandle() method to make a pop up tip for WIN information.
     */
    @Override
    public void collisionHandle(final MovableObject object, Context context) {
        if (collisionDetect(object)) {
            object.collisionReact(this);

            new  AlertDialog.Builder(context)
                .setTitle("YOU WIN :)" )
                .setPositiveButton("OK", null)
                .show();

            System.out.println("----- YOU WIN :) -----");
        }
    }
}
