package nameless.jumper;

import android.content.Context;

/**
 * Created by zhang on 2018/5/15.
 *
 * This interface regulars un-operable objects for players.
 * e.g. floor, wall or any other obstructions.
 */

public interface Obstruction {
    /**
     * To check if an object crashed with obstruction.
     * @param object target
     * @return true if crashed.
     */
    boolean collisionDetect(MovableObject object);

     /**
     * This method is used to handle collision "event".
     * @param object the object which crashed with this block
     * @param context comes from Android activity. used for pop up tip.
     */
    void collisionHandle(MovableObject object, Context context);
}
