package nameless.jumper;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import android.os.Handler;


/**
 * Created by zhang on 2018/5/9.
 *
 * This class is the main view of the game.
 * It refreshes every 17ms (about 60FPS)
 */
public class MainGameView extends View implements View.OnTouchListener, Runnable{
    // System Config
    private Context context;

    private Handler timer;
    private Paint paint;

    // Game Content: including
    private Rocket rocket;

    private Block[] basicBlockList;

    private FailBlock failBlock;

    private WinBlock winBlock;

    // Motion constant
    private static final float verticalG = 1.0f; // 重力加速度
    private static final float horizontalG = 0.0f; // 水平加速度，风速？

    public MainGameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        this.setOnTouchListener(this);

        // init system components.
        this.paint = new Paint();

        // start running
        this.timer = new Handler();
        this.timer.postDelayed(this, 17);

        // init game content
        this.rocket = new Rocket(BitmapFactory.decodeResource(getResources(), R.drawable.rocket), 0.0f, 10.0f);
        this.rocket.start();

        this.basicBlockList = new Block[5];

        this.winBlock = new WinBlock(BitmapFactory.decodeResource(getResources(), R.drawable.winblock), 1600.0f, 800.0f);
        this.failBlock = new FailBlock(BitmapFactory.decodeResource(getResources(), R.drawable.failblock), 0.0f, 950.0f);

        this.basicBlockList[0] = new Block(BitmapFactory.decodeResource(getResources(), R.drawable.block),  -50.0f, 800.0f);
        this.basicBlockList[1] = new Block(BitmapFactory.decodeResource(getResources(), R.drawable.block),  300.0f, 800.0f);
        this.basicBlockList[2] = new Block(BitmapFactory.decodeResource(getResources(), R.drawable.block),  650.0f, 800.0f);
        this.basicBlockList[3] = new Block(BitmapFactory.decodeResource(getResources(), R.drawable.block),  1000.0f, 800.0f);
        this.basicBlockList[4] = new Block(BitmapFactory.decodeResource(getResources(), R.drawable.block),  1300.0f, 800.0f);
    }

    /**
     * Callback when touched
     */
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        float xTouched = motionEvent.getX();
        float yTouched = motionEvent.getY();

        switch (motionEvent.getAction()) {
            // Firstly pressed down
            case MotionEvent.ACTION_DOWN:
                System.out.println("DOWN");
                float factorX = (xTouched - rocket.getCenterX()) / 1800;
                float factorY = Math.max((rocket.getCenterY() - yTouched) / 1020, 0); // max = 1020?
                rocket.start(factorX, factorY);
                break;
            // Keep pressing and move
            case MotionEvent.ACTION_MOVE:
                System.out.println("MOVE");
                break;
            // Release
            case MotionEvent.ACTION_UP:
                System.out.println("UP");
                break;
            // ?
            case MotionEvent.ACTION_CANCEL:
                System.out.println("CANCEL");
                break;
            default:
                System.out.println("UNKNOWN");
        }
        return true;
    }

    /**
     * Draw frames
     */
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap(failBlock.getBitmap(), failBlock.getCurrentX(), failBlock.getCurrentY(), paint);

        for (Block block : basicBlockList) {
            canvas.drawBitmap(block.getBitmap(), block.getCurrentX(), block.getCurrentY(), paint);
        }

        canvas.drawBitmap(winBlock.getBitmap(), winBlock.getCurrentX(), winBlock.getCurrentY(), paint);

        canvas.drawBitmap(rocket.getBitmap(), rocket.getCurrentX(), rocket.getCurrentY(), paint);
    }

    @Override
    public void run() {
        rocket.step(verticalG, horizontalG);

        if (rocket.isMoving()) {
            winBlock.collisionHandle(rocket, context);
            failBlock.collisionHandle(rocket, context);

            for (Block block : basicBlockList) {
                block.collisionHandle(rocket, context);
            }
        }

        this.invalidate();
        timer.postDelayed(this, 17);
    }
}
