#  Jumper - A Simple RetroGame

A simple retrogame implement based on Android 8.0.

This project is built for a group assignment of COMP6442 Software Construction.

## Content List
* [Statement of Originality](#section_origin)
* [Members](#section_members)
* [Design Summary](#section_design)
* [UML Diagram](#section_uml)

#### Supervisor
* Dr Eric McCreath

#### Student
<a id="section_members"></a>
+ Yu Zhang u6297321 - team leader, help page code, morale builder
+ Xinlin Li u6460036- note taker, documentation checker, user input
+ Jiawei Gong u5955430 - tricky android code, game state, game graphics rendering

## Statement of Originality
<a id="section_origin"></a>
(also see in the Wiki)

We declare that the work we have submitted for Stage B of this assignment and all stages before it is entirely our own work.

Signed: Yu Zhang(u6297321) Jiawei Gong(u5955430) Xinlin Li(u6460036)

## Overview

![Game Overview](./images/test.png "Game Overview")

## Design Summary
<a id="section_design"></a>
 In our game, players need to tap the screen to let the little square jump.

 The time of the tap will determine the distance of the jump, which means the longer you tap on the screen, the farther the square jumps. 
 
 During the jumping process , the player should avoid to land on some dangerous place like river and brambles. Also, there will be some other items like spring which can help you jump farther.

 In some levels, players should find keys to unblock the door and continue the adventure.
 The game will have a timer to document the time you use to finish the game. The less time you use, the higher mark you get.

 The game also has different level of difficulty. With the increasing level, the barriers may increase but the mark is also higher.

 We are also trying to improve the user experience by adding an additional mode of operation. We may let players use their voice to control the square.

 The louder your voice is, the farther the square jumps. The final version may be a little different from the design.

## UML Diagram
<a id="section_uml"></a>
![UML](./images/UML.png "UML diagram")

---
### For more information: please see the Wiki of this project :)
